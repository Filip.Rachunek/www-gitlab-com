---
layout: markdown_page
title: After Action Reports
category: Artifacts
---

The following reports are templates to be filed after a particular activity takes place.  These reports should be distributed to, at a minimum, the customer's SAL, the customer's TAM, your manager and the customer's contacts where appropriate.

## On this page
{:.no_toc}

- TOC
{:toc}

This report tempalte should be used for
* On-Site After Action Reports
* Go-Live After Action Report
* Training After Action Report

## Template
```
Particpants:

Customer
----


GitLab 
-----


Goals
* 

Outcomes
*

Next Steps
* 

```